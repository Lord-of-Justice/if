using System;
using System.Linq;

namespace Condition
{
    public static class Condition
    {
        /// <summary>
        /// Implement code according to description of  task 1
        /// </summary>        
        public static int Task1(int n)
        {
            if (n > 0) return n * n;
            else if (n < 0) return n * -1;
            else return n;
        }

        /// <summary>
        /// Implement code according to description of  task 2
        /// </summary>  
        public static int Task2(int n)
        {
            string s = n.ToString();
            var numList = s.Select(x => Convert.ToInt32(x.ToString())).OrderByDescending(x => x).ToList();
            return Int32.Parse(string.Join("", numList));
            //if (n == 401) return 410;
            //if (n == 174) return 741;
            //if (n == 454) return 544;
            //return n;
        }
    }
}
