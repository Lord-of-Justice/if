﻿using NUnit.Framework;
using System;

namespace Condition.Tests
{
    [TestFixture]
    public class Tests
    {
        [Test]
        public void Pass()
        {
            Assert.Pass();
        }
        [Test]
        public void Task2_ReturnCorrectValue()
        {
            Check(401, 410);
            Check(999, 999);
            Check(174, 741);
            Check(454, 544);

            void Check(int n, int result)
            {
                var res = Condition.Task2(n);
                Assert.AreEqual(res, result, message: "Task2 method implementation works incorrectly ");
            }

        }

    }
}
